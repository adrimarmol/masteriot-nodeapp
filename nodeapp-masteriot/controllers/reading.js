// Load required packages
var Reading = require('../models/reading');

// Recoge los datos de la BD de la última hora solo

// Create endpoint /api/sensors for GET
exports.getReadings = function(req, res) {
  console.log('Getting readings...');
  var oneHour = 3600000;
  var now = new Date();
  var timeNow = now.getTime() - oneHour;
  // Use the Sensor model to find all sensor
  Reading.find({timestamp: {$gte: timeNow} }, function(err, readings) {
    if (err)
      res.send(err);

    res.json(readings);
  }).sort({timestamp: -1});
};
